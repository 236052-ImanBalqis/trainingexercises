package filehandling;

import java.io.File;

public class GetfileInformation {

	public static void main(String[] args) {

		File obj = new File("C:\\Users\\Bil\\git\\firstdemo\\mySpace\\file2.txt");
		System.out.println(obj.exists());
		
		if (obj.exists()) {
			System.out.println("Filename: "+obj);
			System.out.println("My file path address: "+obj.getAbsolutePath());
			System.out.println("Check writable or not: " +obj.canWrite());
			System.out.println("Check reable or not: " +obj.canRead());
			System.out.println("File size in bytes: " +obj.length());
		}
	}

}
