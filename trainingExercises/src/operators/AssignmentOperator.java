package operators;

public class AssignmentOperator {
		// = += -= *= /= %=
	public static void main(String[] args) {
		// TODO Auto-generated method stub

			int n1=10;
			int n2=20; // n2 value is 20
			
			n2=n1; //n2 value is 10
			System.out.println(n2);
			n2+=n1;//10+10=20
			System.out.println(n2);
			n2-=n1; //20-10=10
			System.out.println(n2);
			n2*=n1; //10*10=100
			System.out.println(n2);
			n2/=n1; //100/10=10
			System.out.println(n2);
	}

}
