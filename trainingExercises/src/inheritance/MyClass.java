package inheritance;

public class MyClass extends GrandChild{

	public static void main(String[] args) {

		MyClass m = new MyClass();
		m.method2();
		m.method1();
		m.mymethod();
		m.gcmethod();
		
	}

}
