package inheritance;

public class GrandChild extends Child{
	public void gcmethod() {
		System.out.println("GrandChild method logic...");
	}

	public static void main(String[] args) {

		GrandChild g = new GrandChild();
		g.method1();
		g.method2();
		g.mymethod();
	}

}
