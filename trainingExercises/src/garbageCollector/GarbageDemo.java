package garbageCollector;

public class GarbageDemo {
	
	public void finalize() {
		System.out.println("unreference object garbage collected");
	}

	public static void main(String[] args) {
		GarbageDemo gd = new GarbageDemo();
		gd = null;//Nullifying
		GarbageDemo gd2 = new GarbageDemo(); //referencing to another
		gd=gd2;
		//anonymous object
		new GarbageDemo(); // no name
		System.gc();

	}

}
