package thisSuper;

class Animal{
	String color = "White..Parent Class";//parent class instance variable
	
}

public class Dog extends Animal {
	String color = "green...";//child class instance variable
	void printcolor() {
		String color = "Black..."; //local variable
		System.out.println(color);//print local variable
		System.out.println(this.color);// print current class instance variable
		System.out.println(super.color);//
	}

	public static void main(String[] args) {
		
		Dog d = new Dog ();
		d.printcolor();

	}

}
