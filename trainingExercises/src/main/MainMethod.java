package main;

public class MainMethod {
	public static void main() {
		System.out.println("This is regular method....output");
		
	}
	public static void main (int a) {
		System.out.println("This is another main method");
	}

	public static void main(String[] bye) {//execution starts from here
		//public - access modifier access to all\
		//static - call w/o any object
		//void - return type doesnt return any value
		//main - name of method
		//String [] - array of String type arguments
		//args - just a parameter... can change if you wan
		
		System.out.println("This is main method");
		main();
		main(10);

	}

}
