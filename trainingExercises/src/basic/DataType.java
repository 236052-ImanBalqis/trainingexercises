package basic;

public class DataType {
	public static void main(String[] args) {
		int a=20;
		byte b=65;
		short s=200;
		long l=4646464;
		float f =6565.6f;
		double d =4545445454.455d;
		
		System.out.println("Print a value: "+a);
		System.out.println("Print b value: "+b);
		System.out.println("Print s value: "+s);
		System.out.println("Print l value: "+l);
		System.out.println("Print f value: "+f);
		System.out.println("Print d value: "+d);
	}
	

}
