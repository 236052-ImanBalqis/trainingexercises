package collections;

import java.util.HashSet;
import java.util.Set;

public class SetDemo {

	public static void main(String[] args) {

		Set<String> sports = new HashSet<>();
		sports.add("Tennis");
		sports.add("BaseBall");
		sports.add("Kho-Kho");
		sports.add("Khabaddi");
		System.out.println(sports);
		sports.add("Badminton");
		int ts = sports.size();
		System.out.println(ts);
		
		
	}

}
