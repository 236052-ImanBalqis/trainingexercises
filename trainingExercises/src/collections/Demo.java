package collections;

import java.util.ArrayList;
import java.util.List;

public class Demo {

	public static void main(String[] args) {

		List<String> list = new ArrayList<>();
		list.add("NewYork");
		list.add("Rome");
		list.add("Paris");
		list.add("Copenhagen");
		list.add("New Delhi");
		list.add("Malaysia");
		list.add("12345");
		list.add("6789");
		list.add(6, "Stockholm");//add to elements
		list.set(5, "Shanghai"); // to change element
		System.out.println(list);
		System.out.println("Capital city of Denmark: "+list.get(3));//get to print particular element
		list.remove(0);//remove element
		System.out.println(list);
		
		//to add particular position
		list.add(2, "Canada");
		System.out.println(list);
		
		List<String> countries = new ArrayList<>();
		countries.add("India");
		countries.add("Malaysia");
		countries.add("Denmark");
		countries.add("Australia");
		System.out.println(countries);
		
		//add both list and countries
		countries.addAll(3, list);
		System.out.println("After merging list with countries: "+countries);
		
		int tsize = list.size();//to find the size of list
		System.out.println(tsize);
		boolean c = list.contains("12345");//to find whether element exist in list
		System.out.println(c);
		
		
		
	}

}
