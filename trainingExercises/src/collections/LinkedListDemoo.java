package collections;

import java.util.LinkedList;

public class LinkedListDemoo {

	public static void main(String[] args) {

		LinkedList<String> abc = new LinkedList <>();
		abc.add("Apple");
		abc.add("Banana");
		abc.add("Grapes");
		System.out.println(abc.size());
		System.out.println(abc);
		abc.add(2, "Custard Apple");
		System.out.println(abc);
		
		LinkedList<String> xyz = new LinkedList <>();
		xyz.add("Rose");
		xyz.add("Jasmine");
		abc.addAll(xyz);
		System.out.println(abc);
	}

}
