package interfaceDemo;
//multiple inheritance achieved with interface
interface Continent{
	void ContinentName();
}

interface Country{
	void countryName();
	
}
interface City{
	void cityName();
		
}
public class Multi implements Continent, Country, City {


@Override
public void cityName() {
	System.out.println("Copenhangen");
	
}

@Override
public void countryName() {
	System.out.println("Denmark");
	
}

@Override
public void ContinentName() {
	System.out.println("Europe");
	
}


	public static void main(String[] args) {
Multi m = new Multi();
m.cityName();
m.countryName();
m.ContinentName();
	}
}
		
	


