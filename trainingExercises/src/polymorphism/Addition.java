package polymorphism;
// method OVERLOADING
public class Addition {
	void sum(int a, int b) {
		int c = a+b;
		System.out.println("Total of a and b is: "+c);
		
	}
	//overloading method sum
	void sum(int a, int b, int c) {//3 parameters so overloadded 'sum' method
		int d =a+b+c;
		System.out.println("the total a, b and c:"+d);
	}
	// overloading by changing data type
	void sum (double dd, double ff) {
		double rr = dd*ff;
		System.out.println("product of dd and ff is: "+rr);
	}
	public static void main (String[] args) {
		Addition obj = new Addition(); //obj created
		obj.sum(55, 66);// 2 argument
		obj.sum(20, 30, 400);//3 argument
		obj.sum(55.65, 46464.233);
	}
}
